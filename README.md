### docker notes

```
$ docker ps
$ docker ps -a
$ docker images
```

```
$ docker start container_name
$ docker start container_name
```

```
$ docker inspect $ID
$ docker inspect  zealous_wilson | jq -r '.[0].NetworkSettings.SandboxKey'
```

```
$ docker rm $(docker ps -aq)
$ docker rmi $(docker images -q)
$ docker rm $ID
$ docker rmi $ID
```

```
$ docker run -it ubuntu:16.04 bash
$ docker run -it -p 8000:80 ubuntu:16.04 bash
$ docker run -it -p 8000:80 -v $(pwd):/opt ubuntu:16.04 bash
$ docker run --rm -it -p 8000:80 -v $(pwd):/opt ubuntu:16.04 bash
```

```
$ docker diff de7b37945ea3
$ docker commit -a "Kiruban PREMKUMAR" -m "Installed nginx" de7b37945ea3 kiruban/nginx:0.1.0
```

```
$ docker run --rm -it -p 8000:80 -v $(pwd):/var/www/html/ kiruban/nginx:0.1.0 nginx
```

```
# inside the container
echo "daemon off;" >> /etc/nginx/nginx.conf
```

```
$ docker diff fc7e806fe416
```

```
$ docker commit -a "Kiruban PREMKUMAR" -m "Installed nginx" adoring_bhabha kiruban/nginx:0.2.0
$ docker run -it -p 8000:80 -v $(pwd):/var/www/html/ kiruban/nginx:0.2.0 nginx
```

```
$ docker run -d -it -p 8000:80 -v $(pwd):/var/www/html/ kiruban/nginx:0.2.0 nginx
$ docker history kiruban/nginx:0.2.0
```

### Dockerfile example

```
FROM ubuntu:16.04

MAINTAINER Kiruban PREMKUMAR

RUN apt-get update && apt-get install -y nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && echo "daemon off;" >> /etc/nginx/nginx.conf

CMD ["nginx"]
```

### How to build it?

```
$ docker build -t kiruban/nginx:0.1.0 .
```

```
$ docker run -d -p 8000:80 -v $(pwd):/var/www/html kiruban/nginx:0.1.0
```

### Multiple tag

```
$ docker build -t kiruban/nginx:0.2.0 -t kiruban/nginx:latest .
$ docker run -d -p 8000:80 -v $(pwd):/var/www/html kiruban/nginx
```

**latest** is the default tag if you don't specify one!

### build php image

```
$ docker build -t kiruban/php:0.1.0 .
```

### Testing our build

```
$ docker run --rm -it kiruban/php:0.1.0 php -v
$ docker run --rm -it kiruban/php:0.1.0 composer -h
$ docker run --rm -it kiruban/php:0.1.0 which php-fpm7.0
```

### Build V2

```
$ docker build -t kiruban/nginx:0.2.0 .
```

### Run V2

```
$ docker run -d --name=myphp -v $(pwd)/application:/var/www/html kiruban/php:0.1.0
$ docker run -d --link=myphp:php -p 8000:80 -v $(pwd)/application:/var/www/html kiruban/nginx:0.2.0
```

**check phpinfo** at http://localhost:8000

```
$ docker exec -it zealous_bassi bash

# inside container

$ getent hosts php
$ cat /etc/hosts
```

```
$ docker inspect myphp | jq -r '.[0].NetworkSettings.IPAddress'
$ docker inspect zealous_bassi | jq -r '.[0].NetworkSettings.IPAddress'
```

### Use docker hub for public repositories

```
$ docker login
$ docker images
$ docker push kiruban/php:0.1.0
$ docker push kiruban/nginx:0.2.0

$ docker pull kiruban/php:0.1.0
$ docker pull kiruban/nginx:0.2.0
```


### Using official mysql container

```
$ docker run mysql:5.7 --version
```

### Linking redis, mysql, php-fpm & nginx

```
# run redis container as daemon

$ docker run -d --name=redis redis:alpine

# run mysql container as daemon

$ docker run -d --name=mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=my-app -e MYSQL_USER=app-user -e MYSQL_PASSWORD=labiloute mysql:5.7

# run php container as daemon

$ docker run -d --name=php --link=redis:redis --link=mysql:mysql -v $(pwd)/application:/var/www/html kiruban/php:0.1.0

# run nginx container as daemon

$ docker run -d --name=nginx --link=php:php -p 8000:80 -v $(pwd)/application:/var/www/html kiruban/nginx:0.2.0


# Let's check 

$ docker ps
CONTAINER ID        IMAGE                 COMMAND                  CREATED                  STATUS              PORTS                            NAMES
53510796b65b        kiruban/nginx:0.2.0   "nginx"                  Less than a second ago   Up 3 seconds        8000/tcp, 0.0.0.0:8000->80/tcp   nginx
29e6a2557763        kiruban/php:0.1.0     "php-fpm7.0"             2 minutes ago            Up 2 minutes        9000/tcp                         php
5d03e9d18903        mysql:5.7             "docker-entrypoint.s…"   4 minutes ago            Up 4 minutes        3306/tcp                         mysql
c2790500165c        redis:alpine          "docker-entrypoint.s…"   6 minutes ago            Up 7 minutes        6379/tcp                         redis
```

### Creating docker network

```
$ docker network create --driver=bridge kiki-net

$ docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
c8084552fc31        bridge              bridge              local
10c02a93f1eb        host                host                local
b7d29c356f01        kiki-net            bridge              local
7027c4b32dd8        none                null                local
a8a46ba4c298        snakeeyes_default   bridge              local

$ docker network inspect kiki-net | jq
[
  {
    "Name": "kiki-net",
    "Id": "b7d29c356f0137d883108ba6ba8ce404c1942b05311c373bb4bcbee31f89e7a0",
    "Created": "2018-02-04T21:38:46.371414069Z",
    "Scope": "local",
    "Driver": "bridge",
    "EnableIPv6": false,
    "IPAM": {
      "Driver": "default",
      "Options": {},
      "Config": [
        {
          "Subnet": "172.19.0.0/16",
          "Gateway": "172.19.0.1"
        }
      ]
    },
    "Internal": false,
    "Attachable": false,
    "Ingress": false,
    "ConfigFrom": {
      "Network": ""
    },
    "ConfigOnly": false,
    "Containers": {},
    "Options": {},
    "Labels": {}
  }
]

$ docker run -d --name=redis --network=kiki-net redis:alpine

$ docker run -d --name=mysql --network=kiki-net -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=my-app -e MYSQL_USER=app-user -e MYSQL_PASSWORD=labiloute mysql:5.7

$ docker run -d --name=php --network=kiki-net -v $(pwd)/application:/var/www/html kiruban/php:0.1.0

$ docker run -d --name=nginx --network=kiki-net -p 8000:80 -v $(pwd)/application:/var/www/html kiruban/nginx:0.2.0

$ docker network inspect kiki-net | jq ".[0].Containers"
{
  "3652f9b23f8baf1f3ea3be3fc0ac39000b928a4849d677283a9aa9514be07d36": {
    "Name": "redis",
    "EndpointID": "962cfd430ab6cf737d927aabe66c55ee673a4622b46911a5f6fc9c8876ee7b83",
    "MacAddress": "02:42:ac:13:00:02",
    "IPv4Address": "172.19.0.2/16",
    "IPv6Address": ""
  },
  "412f6753feced384999cefb25a2945f578b610fc14c5f3199c8717fad781a9bd": {
    "Name": "mysql",
    "EndpointID": "56e7c6204ef7d1b91fdb3b4bad1d6e69e55998f93b3c6bbc1c674051402f97ac",
    "MacAddress": "02:42:ac:13:00:03",
    "IPv4Address": "172.19.0.3/16",
    "IPv6Address": ""
  },
  "c6bf6a58fb5d25460f36b0d879b8349593c1cfd7be737e7b043118db19386cd5": {
    "Name": "nginx",
    "EndpointID": "2900f104e00dd752f69d08e902068a80814a4fb12326258ba6e4cbe71acffd35",
    "MacAddress": "02:42:ac:13:00:05",
    "IPv4Address": "172.19.0.5/16",
    "IPv6Address": ""
  },
  "ee0131eaeee9157b7b1e87d3bb8ec6acefdd0f4b4e02cd679356fa4ee5972643": {
    "Name": "php",
    "EndpointID": "ca773178faee3596861cd9671cb0a198efc287df8995e538b4d1725a4ab97c3a",
    "MacAddress": "02:42:ac:13:00:04",
    "IPv4Address": "172.19.0.4/16",
    "IPv6Address": ""
  }
}
```

### Docker volumes

```
$ docker volume ls
DRIVER              VOLUME NAME
local               26cca6eecedf599fb308dc61aa3154f54d466314e4275fe94dbd7c8a9d30791a

$ docker volume inspect 26cca6eecedf599fb308dc61aa3154f54d466314e4275fe94dbd7c8a9d30791a
[
    {
        "CreatedAt": "2018-02-04T21:44:04Z",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/26cca6eecedf599fb308dc61aa3154f54d466314e4275fe94dbd7c8a9d30791a/_data",
        "Name": "26cca6eecedf599fb308dc61aa3154f54d466314e4275fe94dbd7c8a9d30791a",
        "Options": {},
        "Scope": "local"
    }
]

$ docker stop $(docker ps -aq)
$ docker rm $(docker ps -aq)
$ docker volume rm $(docker volume ls -q)

$ docker volume create --driver=local mysqldata
# or
$ docker volume create --driver=local --name=mysqldata


$ docker volume inspect mysqldata
[
    {
        "CreatedAt": "2018-02-04T21:58:49Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/mysqldata/_data",
        "Name": "mysqldata",
        "Options": {},
        "Scope": "local"
    }
]

$ docker run --rm -it -v /:/vm-root alpine:latest ls -l /vm-root//var/lib/docker/volumes/mysqldata/_data
total 0
```

# Using custom mysql volume 

```
$ docker run -d --name=redis --network=kiki-net redis:alpine

$ docker run -d --name=mysql --network=kiki-net -v mysqldata:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=my-app -e MYSQL_USER=app-user -e MYSQL_PASSWORD=labiloute mysql:5.7

$ docker run -d --name=php --network=kiki-net -v $(pwd)/application:/var/www/html kiruban/php:0.1.0

$ docker run -d --name=nginx --network=kiki-net -p 8000:80 -v $(pwd)/application:/var/www/html kiruban/nginx:0.2.0
```

# check mysql

```
$ docker run --rm -it -v /:/vm-root alpine:latest ls -l /vm-root//var/lib/docker/volumes/mysqldata/_data
total 188480
-rw-r-----    1 999      ping            56 Feb  4 22:03 auto.cnf
-rw-------    1 999      ping          1675 Feb  4 22:03 ca-key.pem
-rw-r--r--    1 999      ping          1107 Feb  4 22:03 ca.pem
-rw-r--r--    1 999      ping          1107 Feb  4 22:03 client-cert.pem
-rw-------    1 999      ping          1679 Feb  4 22:03 client-key.pem
-rw-r-----    1 999      ping          1323 Feb  4 22:03 ib_buffer_pool
-rw-r-----    1 999      ping      50331648 Feb  4 22:03 ib_logfile0
-rw-r-----    1 999      ping      50331648 Feb  4 22:03 ib_logfile1
-rw-r-----    1 999      ping      79691776 Feb  4 22:03 ibdata1
-rw-r-----    1 999      ping      12582912 Feb  4 22:03 ibtmp1
drwxr-x---    2 999      ping          4096 Feb  4 22:03 my@002dapp
drwxr-x---    2 999      ping          4096 Feb  4 22:03 mysql
drwxr-x---    2 999      ping          4096 Feb  4 22:03 performance_schema
-rw-------    1 999      ping          1679 Feb  4 22:03 private_key.pem
-rw-r--r--    1 999      ping           451 Feb  4 22:03 public_key.pem
-rw-r--r--    1 999      ping          1107 Feb  4 22:03 server-cert.pem
-rw-------    1 999      ping          1675 Feb  4 22:03 server-key.pem
drwxr-x---    2 999      ping         12288 Feb  4 22:03 sys

$ docker logs -f mysql

$ docker exec -ti mysql bash

$ docker stop $(docker ps -aq)
$ docker rm $(docker ps -aq)

$ docker run -d --name=mysql --network=kiki-net -v mysqldata:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=my-app -e MYSQL_USER=app-user -e MYSQL_PASSWORD=labiloute mysql:5.7
```